//UWU
const { Monitor } = require('klasa');

module.exports = class extends Monitor {

    constructor(...args) {
        super(...args, {
            name: 'UwU',
            enabled: true,
            ignoreBots: true,
            ignoreSelf: true,
            ignoreOthers: false,
            ignoreWebhooks: true,
            ignoreEdits: true,
            ignoreBlacklistedUsers: false,
            ignoreBlacklistedGuilds: false
        });
    }

    run(msg) {
      const { content } = msg;
        if ((content.toLowerCase().includes('uwu') === true)||(content.toLowerCase().includes('owo') === true)||(content.includes('ÅwÅ') === true)||(content.includes('ÆwÆ')=== true)) msg.channel.send('UwU')
       }
};
