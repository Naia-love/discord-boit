//just some trans thing lol
const { Monitor } = require('klasa');

module.exports = class extends Monitor {

    constructor(...args) {
        super(...args, {
            name: 'e',
            enabled: true,
            ignoreBots: true,
            ignoreSelf: true,
            ignoreOthers: false,
            ignoreWebhooks: true,
            ignoreEdits: true,
            ignoreBlacklistedUsers: false,
            ignoreBlacklistedGuilds: false
        });
    }

    run(msg) {
        if (msg.content.toLowerCase == 'e'||msg.content.toLowerCase == '**e**'||msg.content.toLowerCase == '*e*'||msg.content.toLowerCase == '***e***') {
        msg.reply('Gibe e!');
        msg.member.eCount++;
        msg.member.lastContent = msg.content.toLowerCase;
      }
      else if (msg.content.toLowerCase == 'yes'||msg.content.toLowerCase == 'no' && (msg.member.lastContent == 'e'||msg.lastContent == '**e**'||msg.lastContent == '*e*'||msg.lastContent == '***e***')) {
        return;
      }
      else {
        msg.member.eCount = 0;
        msg.member.lastContent = msg.content.toLowerCase;
      }
       }
};
