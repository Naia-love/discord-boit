//why did i create that, idk
const { Language, util } = require('klasa');
const { bold, code } = require('discord-md-tags');

module.exports = class extends Language {

	constructor(...args) {
		super(...args);
		this.language = {
			RESOLVER_INVALID_EMOJI: 'i need a custom emoji u dumbass',
			COMMAND_REBOOT_DESCRIPTION: 'To reboot (or crash) the bot',
			COMMAND_REBOOT: 'Rebooting !',
			COMMAND_INVITE: () => [
				`To add ${this.client.user.username} to your discord guild:`,
				`<${this.client.invite}>`,
				util.codeBlock('', [
					'The above link is generated requesting the minimum permissions required to use every command currently.',
					'I know not all permissions are right for every guild, so don\'t be afraid to uncheck any of the boxes.',
					'If you try to use a command that requires more permissions than the bot is granted, it will let you know.'
				].join(' ')),
				'PLease contact <@644633142828990505> if any problem'
			],
			COMMAND_INFO: "Just a bot using Klasa, discord.js Node and other dependency.",
			COMMAND_AVATAR_DESCRITION: "Give your avatar!",
    };
  	}

  };
