require('dotenv').config();
const { Client } = require('klasa');

new Client({
    fetchAllMembers: false,
    prefix: '|', //modify it to set your own prefix
    commandEditing: true,
    readyMessage: (client) => `Successfully initialized. Ready to serve ${client.guilds.size} guilds.`,
    shards: "auto",
    ownerID: "644633142828990505", //paste your own id
    commandLogging: true
}).login();
